import { map } from 'lodash';
import React, { useEffect, useState } from 'react';
import './App.css';


const trikeSize = 3;
const playerOne = 'x';
const playerTwo = 'O';

type coordenates = {
  x: number,
  y: number
}

const listOfMovements = {
  moveToRight: (coords: coordenates) => ({ ...coords, 'y': coords.y + 1 }),
  moveToDown: (coords: coordenates) => ({ ...coords, 'x': coords.x + 1 }),
  moveDiagLeftToRight: (coords: coordenates) => ({ 'x': coords.x + 1, 'y': coords.y + 1 }),
  moveDiagRightToLeft: (coords: coordenates) => ({ 'x': coords.x + 1, 'y': coords.y - 1 }),
}

function App() {

  const [blocks, setBlocks] = useState<string[][]>([[]])
  const [turn, setTurn] = useState<string>(playerOne)
  const [isWinner, setIsWinner] = useState<boolean>(false)

  const buildBlocks = () => {
    let matrix = Array.from({ length: trikeSize }, e => Array(trikeSize).fill(""))
    setBlocks(matrix)
  }

  const checkWinner = (coords: coordenates) => {
    let movements = [
      { move: listOfMovements.moveToRight, start: { ...coords, 'y': 0 } },
      { move: listOfMovements.moveToDown, start: { ...coords, 'x': 0 } }
    ]
    if (coords.x == coords.y) {
      movements.push({ move: listOfMovements.moveDiagLeftToRight, start: { 'x': 0, 'y': 0 } })
    }

    if (trikeSize - 1 - coords.x == coords.y) {
      movements.push({ move: listOfMovements.moveDiagRightToLeft, start: { 'x': 0, 'y': trikeSize - 1 } })
    }


    const getMatchs = (move: Function, startCoords: coordenates) => {
      let count = 0;
      let curCoords: coordenates = startCoords;
      for (let i = 0; i < trikeSize; i++) {
        if (getLetterInCoords(curCoords) == turn) {
          count++;
        }
        curCoords = move(curCoords)
      }
      return count;
    }

    const isWinner = !movements.every((movement) => {
      let matchs = getMatchs(movement.move, movement.start)
      return matchs != trikeSize
    })

    setIsWinner(isWinner);
    return isWinner
  }

  const toggeTurn = () => {
    setTurn(turn == playerOne ? playerTwo : playerOne)
  }

  const markBlock = (coords: coordenates) => {
    if (getLetterInCoords(coords) == "" && !isWinner) {

      setBlockWithNewMark(turn, coords)
      checkWinner(coords)

      if (!checkWinner(coords)) {
        toggeTurn()
      }
    }
  }

  const setBlockWithNewMark = (letter: string, coords: coordenates) => {
    let newBlocks = [...blocks];
    newBlocks[coords.x][coords.y] = turn
    setBlocks(newBlocks)
  }

  const getLetterInCoords = (coords: coordenates) => {
    return blocks[coords.x][coords.y]
  }

  const resetGame = () => {
    buildBlocks()
    setIsWinner(false)
    setTurn(playerOne)
  }
  
  useEffect(() => {
    buildBlocks()
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <h1>{isWinner ? "Winner:" : "Turn:"} {turn}</h1>
        {blocks.map((row, xKey) => {
          let cols: JSX.Element[] = map(row, (col, yKey) => {
            return (<div className="block" onClick={() => markBlock({ x: xKey, y: yKey })}> {col}</div>)

          })
          return (<div className="blockRow"> {cols}</div>)
        })}
        <button onClick={resetGame}>reset game </button>
      </header>
    </div>
  );
}

export default App;
